<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\anggota;
use App\Models\buku;
class BukuController extends Controller
{
    public function tambah_buku(Request $request){
        $judul_buku_database = buku::distinct()->get('judul_buku')->toArray();
        // return json_encode($judul_buku_database);
        for($i = 0; $i < sizeof($judul_buku_database);$i++){
            if(strtolower($request->judul_buku) === strtolower($judul_buku_database[$i]['judul_buku'])){
                // return json_encode($i);
                $buku_database = buku::find($i+1);
                // return json_encode($buku_database);
                $buku_database->stok = $buku_database->stok + $request->stok;
                $buku_database->save();
                return redirect('/tambah_buku')->with(['message' => 'Tambah buku sudah dilakukan']);
            }elseif(strtolower($request->judul_buku) != strtolower($judul_buku_database[$i+1]['judul_buku'])){
                continue;
            }
        }
        buku::create([
            'judul_buku' => $request->judul_buku,
            'stok' => $request->stok
        ]);
        return redirect('/tambah_buku')->with(['message' => 'Tambah buku sudah dilakukan']);
    }

    public function cari_buku(Request $request){
        $buku_database = buku::where('judul_buku','LIKE',"%{$request->judul_buku}%")->get();

        // for($i = 0; $i < sizeof($buku_database);$i++){
        //     if(strtolower($request->judul_buku) == strtolower($judul_buku_database[$i]['judul_buku'])){
        //         $judul[$i] = $judul_buku_database[$i]['judul_buku'];
        //         $stok[$i] = $stok_buku_database[$i]['stok'];
        //     }
        // }
        return view('halaman_pencarian_buku_hasil')->with(['buku_database' => $buku_database]);
    }
}
