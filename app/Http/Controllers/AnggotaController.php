<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\anggota;


class AnggotaController extends Controller
{

    public function RegisterAnggota(Request $request){
        $email_database = anggota::distinct()->get('email')->toArray();
        if($request->password == null){
            return redirect('register')->with(['message' => 'Password tidak boleh kosong']);
        }else{
            for($i = 0; $i < sizeof($email_database);$i++){
                if($request->email == $email_database[$i]['email']){
                    return redirect('register')->with(['message' => 'Email telah terdaftar']);
                }
            }
        }
        anggota::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => $request->password
        ]);
        return redirect('login')->with(['message' => 'Registrasi Berhasil']);
    }

    public function LoginAnggota(Request $request){
        $anggota_login = ['email' => $request->email, 'password' => $request->password];
        $email_database = anggota::distinct()->get('email')->toArray();
        $password_database = anggota::distinct()->get('password')->makeVisible(['password'])->toArray();

        for($i = 0; $i < sizeof($email_database); $i++){
            if($anggota_login['email'] == $email_database[$i]['email'] && $anggota_login['password'] == $password_database[$i]['password']){
                return redirect('home');
            }else{
                echo "<script type='text/javascript'>alert('Email/Password salah');</script>";
            }
        }
        return redirect('login')->with(['message' => 'Login Berhasil']);;
    }
}
