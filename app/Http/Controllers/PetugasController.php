<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\petugas;

class PetugasController extends Controller
{

    public function create(){
        return view('register');
    }
    

    public function login_petugas(Request $request){
        $email_database = petugas::distinct()->get('email')->toArray();
        $password_database = petugas::distinct()->get('password')->makeVisible(['password'])->toArray();

        for($i = 0; $i < sizeof($email_database);$i++){
            if($email_database[$i]['email'] == $request->email and $password_database[$i]['password'] == $request->password){
                return redirect('Admin_home');
            }else{
                echo "<script type='text/javascript'>alert('login gagal');</script>";
                return view('login');
            }
        }
    }

}
