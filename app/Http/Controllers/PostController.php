<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\anggota;

class PostController extends Controller
{

    public function create(){
        return view('register');
    }
    public function RegisterUser(Request $request){
        User::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => $request->password
        ]);
    }
    public function RegisterAnggota(Request $request){
        anggota::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => $request->password
        ]);
    }
}
