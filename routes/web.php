<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\PetugasController;
use App\Http\Controllers\BukuController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function(){
    return view('login');
});

Route::get('/home',function(){
    return view('halaman_utama');
});

Route::get('/Admin_home',function(){
    return view('Admin_home');
});

Route::get('/pencarian',function(){
    return view('halaman_pencarian_buku');
});

Route::get('/register', function(){
    return view('register');
});

Route::get('/login_controller', [AnggotaController::class,'LoginAnggota']);

Route::post('/register_controller', [AnggotaController::class,'RegisterAnggota']);

Route::get('/Admin_login', function(){
    return view('Admin_login');
});

Route::get('/login_admin_controller', [PetugasController::class, 'login_petugas']);

Route::get('/tambah_buku',function(){
    return view('tambah_buku');
});

Route::post('/tambah_buku_controller', [BukuController::class, 'tambah_buku']);

Route::get('/pencarian_controller', [BukuController::class, 'cari_buku']);