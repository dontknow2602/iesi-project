<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeminjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjaman', function (Blueprint $table) {
            $table->increments('peminjamanId');
            $table->timestamps();
            $table->integer('userId')->unsigned();
            $table->integer('bukuId')->unsigned();
            $table->integer('petugasId')->unsigned();
            $table->date('tglPinjam');
            $table->integer('denda');
            $table->foreign('userId')->references('userId')->on('anggota');
            $table->foreign('bukuId')->references('id')->on('buku');
            $table->foreign('petugasId')->references('petugasId')->on('petugas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peminjaman');
    }
}
