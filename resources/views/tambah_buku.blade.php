<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Buku</title>
</head>
<body>
    <h3>Penambahan buku</h3>
    <form action="./tambah_buku_controller" method="post">
    {{ csrf_field() }}
    Judul Buku : <input type="text" name="judul_buku"><br>
    Stok Buku : <input type="number" name="stok" id="stok"><br>
    @if (session('message'))
        <div class="alert alert-success">
            <h5>
                {{ session('message') }}
            </h5>
        </div>
    @endif
    <input type="submit" value="Tambah Buku">
    </form>
    <a href="/Admin_home">Kembali ke halaman home admin</a>
</body>
</html>