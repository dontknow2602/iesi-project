<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hasil Pencarian Buku</title>
</head>
<body>
    <h3>Hasil Pencarian</h3>
        <table>
            <tr>
                <th>No</th>
                <th>Judul buku</th>
                <th>Stok</th>
            </tr>
            @for($i = 0; $i < sizeof($buku_database);$i++)
            <tr>
                <td>{{$i+1}}</td>
                <td>{{$buku_database[$i]->judul_buku}}</td>
                <td>{{$buku_database[$i]->stok}}</td>
            </tr>
            @endfor
        </table>
    <a href="./pencarian"><- Back to pencaran</a>
</body>
</html>
