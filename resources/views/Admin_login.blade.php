<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Login</title>
</head>
<body>
    <form action="/login_admin_controller" method="get">
        {{ csrf_field() }}
        <h1>Admin Login</h1>
        Email : <input type="email" name="email" id="email"> <br>
        Password : <input type="password" name="password" id="password"> <br>
        Don't have account ?, <a href="./register">Register</a> <br>
        <input type="submit" value="Login">
    </form>
</body>
</html>