<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Register</h1>
    <form action="./register_controller" method="post">
    {{ csrf_field() }}
        Nama : <input type="text" name="nama"><br>
        Email : <input type="email" name="email"><br>
        Password : <input type="password" name="password"><br>
        <input type="submit" value="Register" >
    </form>
    @if (session('message'))
        <div class="alert alert-success">
            <h5>
                {{ session('message') }}
            </h5>
        </div>
    @endif
</body>
</html>